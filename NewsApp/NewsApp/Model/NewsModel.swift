//
//  NewsModel.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation


struct News {
    private(set) public var source : [String : Any]
    private(set) public var author : String
    private(set) public var title : String
    private(set) public var description : String
    private(set) public var url : String
    private(set) public var urlToImage : String
    private(set) public var publishedAt : String
    private(set) public var content : String

    
    
    init(source : [String : Any], author : String, title: String, description : String, url: String, urlToImage: String, publishedAt: String, content: String){
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
    }
}
