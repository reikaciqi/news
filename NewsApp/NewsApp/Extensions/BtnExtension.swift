//
//  BtnExtension.swift
//  NewsApp
//
//  Created by Rei on 11/7/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import UIKit


extension UIButton {
       func updateAppearence(){
  
         layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
         layer.borderWidth = 1
     }
     
    func removeBorder(){
        self.layer.borderWidth = 0
    }
}



