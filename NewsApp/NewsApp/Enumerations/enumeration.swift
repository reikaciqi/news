//
//  enumeration.swift
//  NewsApp
//
//  Created by Rei on 11/1/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation

enum categoriesToChoose : String{
    case entertainment
    case science
    case business
    case general
    case health
    case technology
    case sport
}
