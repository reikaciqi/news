//
//  SceneDelegate.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Firebase
import BackgroundTasks
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

  
         guard let windowScene = (scene as? UIWindowScene) else { return }
      

            self.window = UIWindow(windowScene: windowScene)
        if Auth.auth().currentUser != nil {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            guard let rootVC = storyboard.instantiateViewController(identifier: "tabBarVC") as? UITabBarController else {
                print("NewsVC not found")
                return
            }
            let rootNC = UINavigationController(rootViewController: rootVC)
            self.window?.rootViewController = rootNC
            self.window?.makeKeyAndVisible()
        } else {
             let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                       guard let rootVC = storyboard.instantiateViewController(identifier: "LoginVC") as? LogInVC else {
                           print("LoginVC not found")
                           return
                       }
                       let rootNC = UINavigationController(rootViewController: rootVC)
                       self.window?.rootViewController = rootNC
                       self.window?.makeKeyAndVisible()

        }
        
    }
    

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
 
          UIApplication.shared.applicationIconBadgeNumber = 0
          
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
//    scheduleAppRefresh()
    }
    
    
//      
//    func scheduleAppRefresh() {
//        let request = BGAppRefreshTaskRequest(identifier: "com.SO.news")
//        request.earliestBeginDate = Date(timeIntervalSinceNow: 30)
//           print("hapi 3")
//        do {
//            try BGTaskScheduler.shared.submit(request)
//        } catch {
//            print("Could not schedule app refresh: \(error)")
//        }
//    }
    

    
    
}

