//
//  AppDelegate.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var time: Double!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UITabBar.appearance().barTintColor = #colorLiteral(red: 0.1215686275, green: 0.2823529412, blue: 0.5843137255, alpha: 1)
        UITabBar.appearance().tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        FirebaseApp.configure() 

      
        
        let twoHours = 7200.0 // 7200 sekonda = 2 ore
        
        time = twoHours
        
          UIApplication.shared.setMinimumBackgroundFetchInterval(time)
          RegistrationService.instance.notificationTime = Int(twoHours)
           

        
        return true
    }
    
    


    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult)-> Void){
        NotificationService.instance.getRecentNews(){
            (success) in
            if success {
                print("background task")
                UIApplication.shared.applicationIconBadgeNumber = 1
               // UIApplication.shared.applicationIconBadgeNumber = NotificationService.instance.counter
                completionHandler(.newData)

                //kushti me poshte per arsye testimi
                if NotificationService.instance.counter > 0 || 1 == 1{
                self.createNotification(newsNr: 3)
               //self.createNotification(newsNr: NotificationService.instance.counter)
                }

            }
          }

       }

    func createNotification(newsNr : Int){
        let center = UNUserNotificationCenter.current()
         
        center.getNotificationSettings { settings in
            guard settings.authorizationStatus == .authorized else { return }
              
                    if settings.alertSetting == .enabled {
                    let content = UNMutableNotificationContent()
                    content.title = "New News"
                    content.body = "You have \(newsNr) new News!"
                    
                    let date = Date().addingTimeInterval(5)
                    let dateComponents = Calendar.current.dateComponents([.year, .month, . day, .hour, .minute, .second], from: date)
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
                    let uuidString = UUID().uuidString
                    
                    let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
                    
                    center.add(request) { (error) in
                        if (error != nil) {
                            print(error!)
                        }
                    }
            }
        }
        
        
        
        
    
    }
    
    
}

