//
//  AllNewsViewCell.swift
//  NewsApp
//
//  Created by Rei on 10/31/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class AllNewsViewCell: UITableViewCell {

    
    @IBOutlet weak var myImage: UIImageView!
    
    @IBOutlet weak var myTitle: UILabel!
    
    @IBOutlet weak var myDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func updateAllNewsCell(news : News){
            let url = URL(string: news.urlToImage)
            myImage.kf.setImage(with: url)
            
            myTitle.text = news.title
            myDescription.text = news.description    }

}
