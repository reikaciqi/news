//
//  NewsViewCell.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Kingfisher
class NewsViewCell: UITableViewCell {


    @IBOutlet weak var myImage: UIImageView!
    
    @IBOutlet weak var myTitle: UILabel!
    
    @IBOutlet weak var myDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateNewsCell(news: News){
        
        
        let url = URL(string: news.urlToImage)
        myImage.kf.setImage(with: url)
        
        myTitle.text = news.title
        myDescription.text = news.description
        
    }

}
