//
//  AllNewsVC.swift
//  NewsApp
//
//  Created by Rei on 10/31/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import SafariServices

class AllNewsVC: UIViewController {

    @IBOutlet weak var allNewsTableView: UITableView!
    var refresher : UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        NewsService.instance.getAllNews(){
            (success) in
            if success {
                self.allNewsTableView.dataSource = self
                self.allNewsTableView.delegate = self
                self.allNewsTableView.reloadData()
                
            }
        }
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(AllNewsVC.refreshAllNews), for: UIControl.Event.valueChanged)
        allNewsTableView.addSubview(refresher)
    }
    
    @objc func refreshAllNews(){
        NewsService.instance.getAllNews(){
            (success) in
            if success {
                self.allNewsTableView.reloadData()
                self.refresher.endRefreshing()
             }
            else {
                self.refresher.endRefreshing()
            }
        }
    }
}


extension AllNewsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsService.instance.allNews.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "allNewsCell") as? AllNewsViewCell{
            let news = NewsService.instance.allNews[indexPath.row]
            cell.updateAllNewsCell(news:news)
            return cell
          }
          else {
            return AllNewsViewCell()
          }
      }
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let newsUrl = NewsService.instance.allNews[indexPath.row].url
           if let url = URL(string: newsUrl){
           openNewsWithSafari(url: url)
          }
      }
    
    func openNewsWithSafari(url: URL){
          let config = SFSafariViewController.Configuration()
          config.entersReaderIfAvailable = true
          let vc = SFSafariViewController(url: url, configuration: config)
          present(vc, animated: true)
      }
      
      
      
        
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//            let height = allNewsTableView.bounds.height/5
//
//            return height
//        }
        
}

extension AllNewsVC {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == NewsService.instance.allNews.count - 1 {
            if NewsService.instance.allNews.count < NewsService.instance.totalNews!{
                NewsService.instance.allNewsPageNumber += 1
                NewsService.instance.getAllNews(){
                    (success) in
                    if success {
                        self.allNewsTableView.reloadData()
                    }
                }
            }
        }
    }
}
