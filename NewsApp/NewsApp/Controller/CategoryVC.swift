//
//  CategoryVC.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {

    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var entertainment: UIButton!
    
    @IBOutlet weak var general: UIButton!
    
    @IBOutlet weak var health: UIButton!
    
    @IBOutlet weak var science: UIButton!
    
    @IBOutlet weak var sport: UIButton!
    
    @IBOutlet weak var technology: UIButton!
    
    @IBOutlet weak var business: UIButton!
    
    @IBOutlet weak var nextCategoryBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        RegistrationService.instance.myCategory = ""
      self.navigationController?.setNavigationBarHidden(false, animated: true)
        categoryLbl.text = NSLocalizedString("choosecategory", comment: "category")
        nextCategoryBtn.setTitle(NSLocalizedString("nextBtn", comment: "next button"), for: .normal)
            }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    
    @IBAction func nextCategoryBtnClicked(_ sender: Any) {
        if RegistrationService.instance.myCategory == ""{
       validationAlert(messageToDispaly: NSLocalizedString("shouldchoosecategory", comment: "choose category"))
        }
    }
    
    @IBAction func entertainmentClicked(_ sender: Any) {
        setCategory(category:"entertainment")
     
        entertainment.updateAppearence()
    }
    
    @IBAction func generalBtnClicked(_ sender: Any) {
        
         setCategory(category: "general")
        
          general.updateAppearence()
        
    }
   
    @IBAction func healthBtnClicked(_ sender: Any) {
        setCategory(category: "health")
          health.updateAppearence()
    }
    @IBAction func scienceBtnClicked(_ sender: Any) {
        setCategory(category: "science")
          science.updateAppearence()
        
    }
    
    @IBAction func sportBtnClicked(_ sender: Any) {
        
        setCategory(category: "sport")
        
        sport.updateAppearence()
    }
    
    @IBAction func technologyBtnClicked(_ sender: Any) {
        setCategory(category: "technology")
              technology.updateAppearence()
        
    }
    
    @IBAction func businessBtnClicked(_ sender: Any) {
         setCategory(category: "business")
              business.updateAppearence()
        
    }
    
    
    func setOrRemove(category: String){
    RegistrationService.instance.getCategory(category: category)
        
    }
    
    func setCategory(category: String){
        entertainment.removeBorder()
        general.removeBorder() 
        health.removeBorder()
        science.removeBorder()
        sport.removeBorder()
        technology.removeBorder()
        business.removeBorder()
        
        setOrRemove(category: category)
       
        
    }
    
    
    func validationAlert(messageToDispaly: String){
           let alert = UIAlertController(title:NSLocalizedString("warning", comment: "warning"), message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil ))
                   self.present(alert, animated: true, completion: nil)
       }
    
}






