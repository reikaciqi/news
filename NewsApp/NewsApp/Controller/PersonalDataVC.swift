//
//  PersonalDataVC.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Firebase

class PersonalDataVC: UIViewController{
   

    @IBOutlet weak var personalDataLbl: UILabel!
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var lastname: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var toLoginBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.setNavigationBarHidden(true, animated: true)
        personalDataLbl.text = NSLocalizedString("personalData", comment: "Personal data")
        name.placeholder = NSLocalizedString("name", comment: "name")
        lastname.placeholder = NSLocalizedString("lastname", comment: "lastname")
        email.placeholder = NSLocalizedString("emailTextField", comment: "email")
        password.placeholder = NSLocalizedString("passTextField", comment: "password")
        confirmPassword.placeholder = NSLocalizedString("confirmPass", comment: "confirm your password")
        messageLbl.text = NSLocalizedString("messageLbl", comment: "have an account?")
        toLoginBtn.setTitle(NSLocalizedString("toLoginBtn", comment: "LogIn"), for: .normal)
        nextBtn.setTitle(NSLocalizedString("nextBtn", comment: "next button"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func nextBtn1Clicked(_ sender: Any) {
     guard
        let name = name.text, !name.isEmpty,
        let lastname = lastname.text, !lastname.isEmpty,
        let email = email.text, !email.isEmpty,
        let password = password.text, !password.isEmpty,
        let confirmPass = confirmPassword.text, !confirmPass.isEmpty else{
            validationAlert(messageToDispaly: NSLocalizedString("allFieldsRequired", comment: "all fields are required"))
            return
        }
        validation()
        RegistrationService.instance.getThePersonalData(name: name, lastname: lastname, email: email, password: password)

        performSegue(withIdentifier: "toCategoryVC", sender: nil)
        
    }
    
    
    func validation(){
        
       
          let characterSet = CharacterSet.letters
        if let textName = name.text {
        if  textName.rangeOfCharacter(from: characterSet.inverted) != nil {
            self.validationAlert(messageToDispaly: NSLocalizedString("nameonlyletters", comment: "name must contain only letters"))
            }
        }
        
        if let textLastname = lastname.text {
            if textLastname.rangeOfCharacter(from: characterSet.inverted) != nil{
                validationAlert(messageToDispaly: NSLocalizedString("lastnameonlyletters", comment: "lastname must contain only letters"))
            }
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: email.text) {
            validationAlert(messageToDispaly: NSLocalizedString("emailnotvalid", comment: "email not valid"))
        }
        
        if let textPassword = password.text {
            if textPassword.count < 8 {
                validationAlert(messageToDispaly: NSLocalizedString("passwordminimumcharacters", comment: "password too short"))
            }
            let confirmPass = confirmPassword.text
            if textPassword != confirmPass {
                validationAlert(messageToDispaly: NSLocalizedString("passdoesnotmatch", comment: "password not matching"))
            }
        }
        
        
        
    }
    
    func validationAlert(messageToDispaly: String){


           let alert = UIAlertController(title:NSLocalizedString("warning", comment: "warning"), message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil ))
                   self.present(alert, animated: true, completion: nil)
       }


    @IBAction func toLoginBtnClicked(_ sender: Any) {
        
      
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LogInVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
}




    
    
    
    
    
    

