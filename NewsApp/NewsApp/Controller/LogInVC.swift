//
//  LogInVC.swift
//  NewsApp
//
//  Created by Rei on 11/12/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Firebase

class LogInVC: UIViewController {

    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var toRegistrationBtn: UIButton!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        loginBtn.setTitle(NSLocalizedString("login", comment: "Log In the user"), for: .normal)
        questionLbl.text = NSLocalizedString("DontHaveAccount", comment: "dont have account?")
        toRegistrationBtn.setTitle(NSLocalizedString("create", comment: "create account"), for: .normal)
        email.placeholder = NSLocalizedString("emailTextField", comment: "email")
        password.placeholder = NSLocalizedString("passTextField", comment: "password")
        loginLbl.text = NSLocalizedString("loginLabel", comment: "log in") 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func registerBtnClicked(_ sender: Any) {

    }
   
  
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        if  email.text != "" && password.text != "" {
        
        
        if let textEmail = email.text , email.text != nil {
             let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
                    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
                    if !emailTest.evaluate(with: textEmail) {
                        validationAlert(messageToDispaly:NSLocalizedString("emailnotvalid", comment: "email not vlid"))
                      return
                  }
              }
              
              guard let textPass = password.text , password.text != "" else {
              return
              }
              
            LogInService.instance.logInUser(email: email.text!, password: textPass) { (success) in
                if success {
                    UpdateUserData.instance.getCredentials { (success) in
                        self.performSegue(withIdentifier: "toTabBar", sender: nil)
                    }
                    
                } else {
                    self.validationAlert(messageToDispaly: NSLocalizedString("incorrectCredentials", comment: "incorrect credentials"))
                }
            }
            
        } else {
            self.validationAlert(messageToDispaly: NSLocalizedString("allFieldsRequired", comment: "all fields are required"))
        }
    }
    func validationAlert(messageToDispaly: String){


              let alert = UIAlertController(title:NSLocalizedString("warning", comment: "warning"), message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
                  alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil ))
                  self.present(alert, animated: true, completion: nil)
          }
}
