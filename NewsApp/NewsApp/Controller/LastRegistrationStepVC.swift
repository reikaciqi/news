//
//  LastRegistrationStepVC.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class LastRegistrationStepVC: UIViewController {

    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var confirmationImage: UIImageView!
    @IBOutlet weak var notificationLbl: UILabel!
    @IBOutlet weak var chooseBtn: MyButtons!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNotificationSettings()
       self.navigationController?.setNavigationBarHidden(false, animated: true)
        
         NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        registerBtn.setTitle(NSLocalizedString("registerBtn", comment: "register"), for: .normal)
        notificationLbl.text = NSLocalizedString("allownotifications", comment: "allow notifications")
        chooseBtn.setTitle(NSLocalizedString("chooseBtn", comment: "choose"), for: .normal)
    }
    
    @objc func willEnterForeground(){
        checkAuthorizationStatus()
    }
    
    @IBAction func enable(_ sender: Any) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.alert, .badge, .sound]) {
            granted, error in
            if (error == nil) {
                 self.getNotificationSettings()
            } else {
                self.checkAuthorizationStatus()
            }
        }
    }
    
    
        func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        switch settings.authorizationStatus {
        case .authorized, .provisional:
            DispatchQueue.main.async {
                self.notificationAlert(messageToDispaly:NSLocalizedString("notificationsAreEnabled", comment: "enabled"))
            }
        case .denied:
            DispatchQueue.main.sync {
            self.notificationAlert(messageToDispaly: NSLocalizedString("notificationsAreDisabled", comment: "disabled"))
            }
        case .notDetermined:
          print("not determined, ask user for permission now")
        @unknown default:
            print("error")
        }
      }
    }
    
 
    @IBAction func registerBtnClicked(_ sender: Any) {
        
           let center = UNUserNotificationCenter.current()
                               
              center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus == .notDetermined {
                    DispatchQueue.main.async{
                    self.validationAlert(messageToDispaly: NSLocalizedString("enable/disable", comment: "enable or disable"))
                    }
                    
                } else {
                    RegistrationService.instance.registerUser(){
                        (success) in
                            if success {
                            self.performSegue(withIdentifier: "toNews", sender: nil)
                                    }
                            else {
                            DispatchQueue.main.async {
                            self.validationAlert(messageToDispaly: NSLocalizedString("registrationFail", comment: "registration failed"))
                                 }
                               }
                            }
                         }
                    })
               }
    
    func checkAuthorizationStatus(){
             let center = UNUserNotificationCenter.current()
                            
           center.getNotificationSettings(completionHandler: { settings in
                  switch settings.authorizationStatus {
                  case .authorized, .provisional:
                    DispatchQueue.main.async {
                        self.confirmationImage.image = UIImage(named: "allowed")
                        self.confirmationImage.isHidden = false
                    }
                  case .denied:
                    DispatchQueue.main.async {
                        self.confirmationImage.image = UIImage(named: "not_allowed")
                        self.confirmationImage.isHidden = false
                    }
                 
                  case .notDetermined:
                    print("not determined")
                    DispatchQueue.main.async {
                        self.confirmationImage.isHidden = true
                    }
                   @unknown default:
                    print("unknown")
                      }
                   })
                 }
    func validationAlert(messageToDispaly: String){


            let alert = UIAlertController(title:NSLocalizedString("warning", comment: "warning"), message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil ))
                self.present(alert, animated: true, completion: nil)
        }
    
    func notificationAlert(messageToDispaly: String){
        let alert = UIAlertController(title:NSLocalizedString("atention", comment: "atention"), message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,  handler: { (action: UIAlertAction!) in
            self.openSettings()
        }))
            self.present(alert, animated: true, completion: nil)
        }
      
    func openSettings(){
        let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .alert)

         let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                 return
             }

             if UIApplication.shared.canOpenURL(settingsUrl) {
                 UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                     print("Settings opened: \(success)") // Prints true
                 })
             }
         }
         alertController.addAction(settingsAction)
         let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
         alertController.addAction(cancelAction)

         present(alertController, animated: true, completion: nil)
        }

}
