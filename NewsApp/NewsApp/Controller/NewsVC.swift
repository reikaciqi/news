//
//  NewsVC.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import SafariServices


@IBDesignable
class NewsVC: UIViewController {
    
    @IBOutlet weak var newsTableView: UITableView!
    var refresher : UIRefreshControl!

    override func prepareForInterfaceBuilder() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.navigationController?.setNavigationBarHidden(true, animated: true)
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Looking for news!")
        refresher.addTarget(self, action: #selector(NewsVC.refreshNews), for: UIControl.Event.valueChanged)
        newsTableView.addSubview(refresher)
        UpdateUserData.instance.getCredentials { (success) in
            if success {
                NewsService.instance.getCategorizedNews(){
                    (success) in
                   if success {
                    self.newsTableView.dataSource = self
                    self.newsTableView.delegate = self
                    self.newsTableView.reloadData()
                                      
                    }
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        newsTableView.reloadData()
        NotificationService.instance.checkForNewNews()  
             }
    
    @objc func refreshNews(){
        NewsService.instance.getCategorizedNews(){
            (success) in
            if success{
                self.newsTableView.reloadData()
                self.refresher.endRefreshing()
            } else {
                self.refresher.endRefreshing()
            }
        }
    }
}

extension NewsVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return NewsService.instance.myNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell =  tableView.dequeueReusableCell(withIdentifier: "newsCell") as? NewsViewCell{
            let news = NewsService.instance.myNews[indexPath.row]
            cell.updateNewsCell(news: news)
            return cell
        }
        else
        {
            return NewsViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsUrl = NewsService.instance.myNews[indexPath.row].url
        if let url = URL(string: newsUrl) {
           openNewsWithSafari(url: url)
        }
    }
    
    
    func openNewsWithSafari(url: URL){
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        let vc = SFSafariViewController(url: url, configuration: config)
        present(vc, animated: true)
    }
    
  
}
