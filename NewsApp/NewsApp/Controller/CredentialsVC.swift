//
//  CredentialsVC.swift
//  NewsApp
//
//  Created by Rei on 10/31/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
import Firebase

class CredentialsVC: UIViewController {

  
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var time: UITextField!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var saveTimeBtn: UIButton!
    
    @IBOutlet weak var logOut: UIButton!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    
    //outlets for category buttons
    
    @IBOutlet weak var entertainment: MyButtons!
    @IBOutlet weak var science: UIButton!
    @IBOutlet weak var business: UIButton!
    @IBOutlet weak var general: UIButton!
    @IBOutlet weak var health: UIButton!
    @IBOutlet weak var technology: UIButton!
    @IBOutlet weak var sport: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkAuthorizationStatus()
        getCredentials()
        saveTimeBtn.isHidden = true
        displaySelectedCategory()
        
        time.addTarget(self, action: #selector(CredentialsVC.timeTextFieldChange(_:)), for: UIControl.Event.editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
}


// signing out
extension CredentialsVC{
    @IBAction func logoutBtnClicked(_ sender: Any) {
        UIApplication.shared.unregisterForRemoteNotifications()
         let firebaseAuth = Auth.auth()
        do{
            try firebaseAuth.signOut()
            let loginVC = storyboard?.instantiateViewController(identifier: "LoginVC") as! LogInVC
            self.navigationController?.pushViewController(loginVC, animated: true)
        } catch let signOutError as NSError{
            print("error signing out: %@", signOutError)
        }
        
     }
}


    //extension per ndryshimin e te dhenave personale
extension CredentialsVC {
    
    func getCredentials(){
         
         let userData = UpdateUserData.instance.dataUser
             name.placeholder = userData["name"] as? String
             lastname.placeholder = userData["lastname"] as? String
             email.placeholder = userData["email"] as? String
             self.oldPassword.placeholder = "Old Password"
             self.newPassword.placeholder = "New Password"
     }
    
   
    
    
     func clearFields(){
         name.text = ""
         lastname.text = ""
         email.text = ""
         oldPassword.text = ""
         newPassword.text = ""
     }
    
    
       @IBAction func saveBtnClicked(_ sender: Any) {
            
            var dataParameters = [String : String]()
         
            let characterSet = CharacterSet.letters
        if let textName = name.text, name.text != "" {
            if textName.rangeOfCharacter(from: characterSet.inverted) != nil {
              self.validationAlert(messageToDispaly: "Name must contain only letters!")
          }
            else {
            dataParameters["name"] = textName
            }
          }
        
        if let textLastname = lastname.text, lastname.text != "" {
              if textLastname.rangeOfCharacter(from: characterSet.inverted) != nil{
                 validationAlert(messageToDispaly: "Lastname must contain only letters!")
              } else {
                dataParameters["lastname"] = textLastname
            }
          }
          
        if let textEmail = email.text , email.text != ""{
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
          let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
          if !emailTest.evaluate(with: textEmail) {
              validationAlert(messageToDispaly: "This email is not valid!")
          } else {
              dataParameters["email"] = textEmail
            }
        
        }
        
        if let textPassword = oldPassword.text , oldPassword.text != "",
            let newPass = newPassword.text, newPassword.text != ""
            {
                 if textPassword.count < 8 {
                     validationAlert(messageToDispaly: "Password must contain 8 or more characters!")
                 }
             
                if  newPass.count < 8  {
                     validationAlert(messageToDispaly: "Your new password must contain 8 or more characters!")
                 } else {
             
                     let userData = UpdateUserData.instance.dataUser
                    if let oldPass = userData["password"] as? String {
                        if oldPass == textPassword {
                        RegistrationService.instance.password = newPass
                        dataParameters["password"] = newPass
                        } else {
                        validationAlert(messageToDispaly:"Wrong old password!")
                        }
                    }
                }
             }
        
            
            if dataParameters.count != 0 {
                UpdateUserData.instance.updateUserCredentials(dataParameters: dataParameters){
                (success) in
                if success{
                UpdateUserData.instance.getCredentials() {
                 (success) in if success{
                 self.getCredentials()
                 self.clearFields()
                    self.validationAlert(messageToDispaly: "Data changed!")
                 }
                 else {
                    print("error")
                    
                    }
                                             
                    }
                                         
                } else {
                print("nuk u be update")
                }
            }
        }
    }
}
    
// extension per ndryshimin e kategorise
extension CredentialsVC{
    func displaySelectedCategory(){
             
        let myCategory = UpdateUserData.instance.dataUser["category"] as! String
           
           switch myCategory {
           case categoriesToChoose.entertainment.rawValue:
              entertainment.updateAppearence()
            
           case categoriesToChoose.business.rawValue:
              business.updateAppearence()
               
           case categoriesToChoose.general.rawValue:
              general.updateAppearence()
               
           case categoriesToChoose.health.rawValue:
              health.updateAppearence()
               
           case categoriesToChoose.science.rawValue:
              science.updateAppearence()
               
           case categoriesToChoose.sport.rawValue:
              sport.updateAppearence()
               
           case categoriesToChoose.technology.rawValue:
              technology.updateAppearence()
           default:
              return
           }
       }
    
    
    @IBAction func updateCategory (sender : UIButton){
          
          switch sender.currentTitle {
          case categoriesToChoose.entertainment.rawValue:
              setCategory(category:sender.currentTitle!)
             sender.updateAppearence()

          case categoriesToChoose.science.rawValue:
             setCategory(category:sender.currentTitle!)
            sender.updateAppearence()
              
          case categoriesToChoose.business.rawValue:
             setCategory(category:sender.currentTitle!)
             sender.updateAppearence()
              
          case categoriesToChoose.general.rawValue:
           setCategory(category:sender.currentTitle!)
        sender.updateAppearence()

          case categoriesToChoose.health.rawValue:
              setCategory(category:sender.currentTitle!)
             sender.updateAppearence()

          case categoriesToChoose.technology.rawValue:
               setCategory(category:sender.currentTitle!)
                sender.updateAppearence()
              
          case categoriesToChoose.sport.rawValue:
              setCategory(category:sender.currentTitle!)
              sender.updateAppearence()
            
          case .none:
              print("none")
          case .some(_):
              print("error")
          }
      }

      
      func setCategory(category: String){
          entertainment.removeBorder()
          general.removeBorder()
          health.removeBorder()
          science.removeBorder()
          sport.removeBorder()
          technology.removeBorder()
          business.removeBorder()
          
          
        UpdateUserData.instance.updateUserCredentials(dataParameters: ["category" : category]) { (success) in
            RegistrationService.instance.getCategory(category: category)
            NewsService.instance.getCategorizedNews(){
                (success) in
                if success {
                    print("news downloaded based in the new category")
                }
            }
        }
    }
}


//extenbsion per ndryshimin e autorizimit te push notifications

extension CredentialsVC {
    
    @objc func willEnterForeground(){
        checkAuthorizationStatus()
    }
    
    @objc func timeTextFieldChange(_ textField: UITextField){
       if textField.text == "" || textField.text == nil {
               saveTimeBtn.isHidden = true
       } else {
               saveTimeBtn.isHidden = false
        }
      }

    func checkAuthorizationStatus(){
          let center = UNUserNotificationCenter.current()
                         
        center.getNotificationSettings(completionHandler: { settings in
               switch settings.authorizationStatus {
               case .authorized, .provisional:
                     print("authorized")
                     DispatchQueue.main.async {
                        self.switchBtn.setOn(true, animated: true)
                        self.time.isHidden = false
                        self.timeLbl.isHidden = false
                        self.time.placeholder = String((UpdateUserData.instance.dataUser["notificationTime"] as! Int/60)/60)
                     
                     }
           
               case .denied:
                DispatchQueue.main.async {
                    self.switchBtn.setOn(false, animated: true)
                    self.timeLbl.isHidden = true
                    self.time.isHidden = true
                }
                
               case .notDetermined:
                print("not determined, ask user for permission now")
                @unknown default:
                 print("unknown")
                            }
                })
    }
    
    @IBAction func changePermissionBtn(_ sender: Any) {
                  openSettings()
                  checkAuthorizationStatus()
          }
       
       func openSettings(){
       let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

           guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
       }
       


     
       @IBAction func saveTimeBtnClicked(_ sender: Any) {
           if time.text != ""{
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.time = (Double(time.text!)!*60)*60
               saveTimeBtn.isHidden = true
               self.time.placeholder = String(Int(appDelegate.time)/60/60)
               self.time.text = ""
            UpdateUserData.instance.updateUserCredentials(dataParameters: ["notificationTime": Int(appDelegate.time)  ]) { (success) in
                if success {
                    self.validationAlert(messageToDispaly: "Time changed successfully!")
                }
            }
           }
           if time.text == "" {
               saveTimeBtn.isHidden = true
           }
       }
   
}


extension CredentialsVC {
    func validationAlert(messageToDispaly: String){


            let alert = UIAlertController(title:"Warning", message: messageToDispaly, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil ))
                    self.present(alert, animated: true, completion: nil)
        }
}
