//
//  UpdateUserData.swift
//  NewsApp
//
//  Created by Rei on 10/31/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Firebase

class UpdateUserData {

static let instance = UpdateUserData()
    var dataUser = NSDictionary()
    
    func getCredentials( completion: @escaping CompletionHandler){
    
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("users").child(uid)
                 ref.observeSingleEvent(of: .value, with: { (snapshot) in
                   // Get user value
                   let value = snapshot.value as? NSDictionary
                    self.dataUser = value!
                    RegistrationService.instance.getCategory(category: value!["category"] as! String)
                    RegistrationService.instance.notificationTime = (value!["notificationTime"] as! Int)
                    
                    RegistrationService.instance.getThePersonalData(name: value!["name"] as! String, lastname: value!["lastname"] as! String, email: value!["email"] as! String, password: value!["password"] as! String)
                    completion(true)
                  
                   }) { (error) in
                     print(error.localizedDescription)
                 }
          

        }
    }
    
    
    
    
    func updateUserCredentials(dataParameters : [String : Any], completion: @escaping CompletionHandler){

        var ref: DatabaseReference!
        if let uid = Auth.auth().currentUser?.uid{
        ref = Database.database().reference()
        ref.child("users").child(uid).updateChildValues(dataParameters)
      completion(true)
        }
    }
}

