

import Foundation
import Firebase

class LogInService {
    
    static let instance = LogInService()
    
   
    
    
    func logInUser(email: String, password: String, completion: @escaping CompletionHandler){
        
        
            Auth.auth().signIn(withEmail: email, password: password) { (AuthDataResult, Error) in
                if (Error != nil) {
                    completion(false)
                }
                else {
                    completion(true)
              
            }
        }
    }
    
    
    
    
    
    
}
