//
//  NotificationService.swift
//  NewsApp
//
//  Created by Rei on 11/4/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NotificationService{
    
    static let instance = NotificationService()
    

    var counter = 0
    
    var newNews = [News]()

    func getRecentNews(completion: @escaping CompletionHandler){
        
        if newNews.count != 0 {
            newNews.removeAll()
        }
    
        
        let parameters = [
                 "country" : "us",
                 "apiKey" : "ecc29e9b8f7e4ddf8ba6bdedad9e6683"
          
        ]
        
        
        AF.request(BASE_URL, method: .get, parameters: parameters , encoder: URLEncodedFormParameterEncoder.default).responseJSON{
           response in
            
            switch response.result {
            case .success(let val):
                
                 let json = JSON(val)
                 let status = json["status"].stringValue
      
                    
                 for (_,subJson):(String, JSON) in json["articles"] {
                        let source = subJson["source"].dictionaryValue
                        let author = subJson["author"].stringValue
                        let title = subJson["title"].stringValue
                        let description = subJson["description"].stringValue
                        let url = subJson["url"].stringValue
                        let urlToImage = subJson["urlToImage"].stringValue
                        let publishedAt = subJson["publishedAt"].stringValue
                        let content = subJson["content"].stringValue
                        
                        let news = News(source: source, author: author, title: title, description: description, url: url, urlToImage: urlToImage, publishedAt: publishedAt, content: content)
                        self.newNews.append(news)
         
                    }
                
                 completion(true)
              
            case .failure(let error):
                print(error)
                completion(false)
            }

        }

    }
    
    
    
    func checkForNewNews(){
        let currentNewsArray = NewsService.instance.allNews
        
        if  let  theDate =  currentNewsArray.first?.publishedAt{
            
      let lastDate = dateConverter(theDate: theDate)


           getRecentNews(){
            (success) in
                  if success {
              
                for item in self.newNews {
                    let recentNewsDate = self.dateConverter(theDate: item.publishedAt)
                    
                    if recentNewsDate > lastDate {
                        self.counter += 1
                    }
                    
                }
                print(self.counter)
            }
            
        }
        
    }
        
}
    
    
    
    func dateConverter(theDate: String) -> Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
     let date = dateFormatter.date(from: theDate)!

        return date
       
    }
    
}
