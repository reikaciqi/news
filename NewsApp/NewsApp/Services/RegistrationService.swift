//
//  RegistrationService.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()
  
class RegistrationService {
    
    static let instance = RegistrationService()
    var uid: String = ""

    var name : String!
    var lastname : String!
    var email : String!
    var password : String!
    
    func getThePersonalData(name: String, lastname: String, email: String, password: String){
        self.name = name
        self.lastname = lastname
        self.email = email
        self.password = password
    }
    
    
    
    var myCategory : String!
    
    func getCategory(category: String){
        myCategory = category
       
    }
    
    var notificationTime : Int?
 
      private var _REF_BASE = DB_BASE
      private var _REF_USERS = DB_BASE.child("users")
      
      var REF_BASE : DatabaseReference {
          return _REF_BASE
      }
      
      var REF_USERS : DatabaseReference {
          return _REF_USERS
      }
      
      
      
      func createDBUser(uid : String, userData: Dictionary<String, Any>){
          REF_USERS.child(uid).updateChildValues(userData)
          
      }
    
       func registerUser( completion: @escaping CompletionHandler){
         Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in 
             guard let user = authResult?.user else {
                
                 completion(false)
                 return
             }
             
            let userData = ["provider": user.providerID,"email" : user.email!, "password": self.password! ,"name" : self.name!, "lastname" : self.lastname!, "category": self.myCategory!, "notificationTime": self.notificationTime!] as [String : Any]
             self.createDBUser(uid: user.uid, userData: userData as Dictionary<String, Any>)
             self.uid = user.uid
             completion(true)
         }
         
     }
    
    
    
    
    
    

    
    
}

