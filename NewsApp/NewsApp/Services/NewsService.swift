//
//  NewsService.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
class NewsService {
    static let instance = NewsService()
    var allNewsPageNumber = 1
    var myNews = [News]()
    var allNews = [News]()
    var totalNews : Int?
    
    
    func getCategorizedNews(completion: @escaping CompletionHandler){
        
        if myNews.count != 0 {
            myNews.removeAll()
        }
        
 
        let category = RegistrationService.instance.myCategory
          
        
        let parameters = [
                 "country" : "us",
                 "category" : category,
                 "apiKey" : "ecc29e9b8f7e4ddf8ba6bdedad9e6683"
        ]
        
        
        AF.request(BASE_URL, method: .get, parameters: parameters , encoder: URLEncodedFormParameterEncoder.default).responseJSON{
           response in
            
            switch response.result {
            case .success(let val):
                
                 let json = JSON(val)
             
                 for (_,subJson):(String, JSON) in json["articles"] {
                        let source = subJson["source"].dictionaryValue
                        let author = subJson["author"].stringValue
                        let title = subJson["title"].stringValue
                        let description = subJson["description"].stringValue
                        let url = subJson["url"].stringValue
                        let urlToImage = subJson["urlToImage"].stringValue
                        let publishedAt = subJson["publishedAt"].stringValue
                        let content = subJson["content"].stringValue
                        
                        let news = News(source: source, author: author, title: title, description: description, url: url, urlToImage: urlToImage, publishedAt: publishedAt, content: content)
                        self.myNews.append(news)
                   
                    }
                
                 completion(true)
              
                  case .failure(let error):
                  print(error)
                  completion(false)
            }

        }

    }
    
    
    
    
    
    
        func getAllNews(completion: @escaping CompletionHandler){
        
//        if allNews.count != 0 {
//            allNews.removeAll()
//        }
            
        let parameters = [
          "country" : "us",
          "apiKey" : "ecc29e9b8f7e4ddf8ba6bdedad9e6683",
          "page": allNewsPageNumber
            
          
            ] as [String : Any]
        
        
        AF.request(BASE_URL, method: .get, parameters: parameters ).responseJSON{
           response in
            
            switch response.result {
            case .success(let val):
                
                 let json = JSON(val)
                      self.totalNews = json["totalResults"].intValue
                 for (_,subJson):(String, JSON) in json["articles"] {
                        let source = subJson["source"].dictionaryValue
                        let author = subJson["author"].stringValue
                        let title = subJson["title"].stringValue
                        let description = subJson["description"].stringValue
                        let url = subJson["url"].stringValue
                        let urlToImage = subJson["urlToImage"].stringValue
                        let publishedAt = subJson["publishedAt"].stringValue
                        let content = subJson["content"].stringValue
                        
                        let news = News(source: source, author: author, title: title, description: description, url: url, urlToImage: urlToImage, publishedAt: publishedAt, content: content)
                        self.allNews.append(news)
                   
                    }
                
                 completion(true)
              
            case .failure(let error):
                print(error)
                completion(false)
            }

        }

    }
    
    
    
    
}
