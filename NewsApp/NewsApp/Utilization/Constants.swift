//
//  Constants.swift
//  NewsApp
//
//  Created by Rei on 10/30/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success : Bool) ->()

let BASE_URL = "https://newsapi.org/v2/top-headlines"


